/* global QUnit */

QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function() {
  "use strict";

  sap.ui.require([
    "com/jssoft/cfui5test/test/integration/AllJourneys"
  ], function() {
    QUnit.start();
  });
});
