sap.ui.define([
  "com/jssoft/cfui5test/controller/BaseController"
], function(Controller) {
  "use strict";

  return Controller.extend("com.jssoft.cfui5test.controller.MainView", {});
});
